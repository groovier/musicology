Musicology is a modern and lightweight Python 3 library for working with musical terms like
notes, scales and chords. In spite of its simplicity, it's still aware of basic and important
musical structures like scale types and key signatures. It also provides basic MIDI functionality
in order to easily 'play' the constructed musical items.