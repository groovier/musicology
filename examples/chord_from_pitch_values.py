
from musicology.note_stack import NoteStack
from musicology.note import Note
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

chord = NoteStack(
    Note.from_pitch_value(48),
    Note.from_pitch_value(52),
    Note.from_pitch_value(59),
    Note.from_pitch_value(62),
    Note.from_pitch_value(67),
)
chord.play()

midi.close_outport()
