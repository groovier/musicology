from musicology.note import Note
from musicology.note_stack import NoteStack
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

root = Note(0)
third = root.transpose_as_new(4)
fifth = third.transpose_as_new(3)
chord = NoteStack(root, fifth, third, root)
print(chord)
chord.play()

midi.close_outport()
