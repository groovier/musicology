from musicology.note import Note
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

notes = []
root = Note(0)
notes.append(root)
third = root.transpose_as_new(4)
notes.append(third)
fifth = third.transpose_as_new(3)
notes.append(fifth)
seventh = fifth.transpose_as_new(3)
notes.append(seventh)
print(notes)
for note in notes:
    print(note.__str__())
    note.play()
for note in notes:
    note.transpose(5)
print(notes)
for note in notes:
    print(note.__str__())
    note.play()

midi.close_outport()
