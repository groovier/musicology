from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

scale = (Scale(0, SCALE_TYPES['major']))
print(scale)
sequence = Sequence(scale=scale)
for note in scale.notes:
    sequence.add_event(note)
sequence.play()

scale = (Scale(5, SCALE_TYPES['major']))
print(scale)
sequence = Sequence(scale=scale)
for note in scale.notes:
    sequence.add_event(note)
sequence.play()

scale = (Scale(11, SCALE_TYPES['major']))
print(scale)
sequence = Sequence(scale=scale)
for note in scale.notes:
    sequence.add_event(note)
sequence.play()

midi.close_outport()
