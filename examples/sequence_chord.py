
from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence, Frame
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

print()
print()
sequence = Sequence(Scale(0, SCALE_TYPES['major']))

sequence.add_event(Frame.from_scale_steps(sequence.scale, (2, 4), (4, 4), (7, 5)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (3, 4), (5, 4), (7, 5)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (1, 4), (4, 4), (6, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (2, 4), (4, 4), (7, 5)))
print(sequence)
sequence.play()

midi.close_outport()