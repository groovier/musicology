
from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence, Frame
from musicology.note import Note
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

sequence = Sequence(Scale(0, SCALE_TYPES['chromatic']))
for pitch_value in [60, 44, 75, 38, 89, 57]:
    frame = Frame()
    note = Note.from_pitch_value(pitch_value)
    frame.add_note(note)
    sequence.add_event(frame)
print(sequence)
print(sequence.scale)
sequence.play()

midi.close_outport()
