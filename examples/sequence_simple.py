from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence, Frame
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

# Scale-based sequence in F Major
sequence = Sequence(Scale(5, SCALE_TYPES['major']))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (0, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (1, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (2, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (3, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (4, 5)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (3, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (2, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (1, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (0, 4)))
print(sequence)
sequence.play()

# Scale_based sequence in E Major
sequence = Sequence(Scale(4, SCALE_TYPES['major']))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (0, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (1, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (2, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (3, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (4, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (3, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (2, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (1, 4)))
sequence.add_event(Frame.from_scale_steps(sequence.scale, (0, 4)))
print(sequence)
sequence.play()

# Chromatic sequence
sequence = Sequence(Scale(0, SCALE_TYPES['major']))
sequence.add_event(Frame.from_chromatic_steps((0, 4)))
sequence.add_event(Frame.from_chromatic_steps((7, 4)))
sequence.add_event(Frame.from_chromatic_steps((4, 4)))
sequence.add_event(Frame.from_chromatic_steps((11, 4)))
sequence.add_event(Frame.from_chromatic_steps((2, 5)))
print(sequence)
sequence.play()

midi.close_outport()
