from musicology.note import Note
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

a = Note(9)
a.play()
d = a.stack(2)
d.play()

midi.close_outport()
