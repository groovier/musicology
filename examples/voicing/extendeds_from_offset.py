from musicology.extended import Extended
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

ext1 = Extended.from_offset(2, dominant=True, ninth='9')
ext1.voice_as_basic()
print(ext1.voicing)
ext1.play()

ext_2 = Extended.from_offset(7, dominant=True, ninth='b9')
ext_2.voice_as_basic()
print(ext_2.voicing)
ext_2.play()

ext_3 = Extended.from_offset(0, eleventh='#11')
ext_3.voice_as_basic()
print(ext_3.voicing)
ext_3.play()

midi.close_outport()
