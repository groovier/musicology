from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence
from musicology.quad import Quad
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

scale = Scale(5, SCALE_TYPES['major'])
s = Sequence(scale, 75)

Quad.from_scale_step('i').set_duration(4).add_to_sequence(s).voice_as_basic(True)
Quad.from_offset(11, minor=True, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_offset(4, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_scale_step('vi').add_to_sequence(s).set_duration(2).voice_as_basic(True)
Quad.from_scale_step('i').set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_scale_step('iv').set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_offset(6, minor=True, dominant=True).set_fifth(6).set_duration(2).add_to_sequence(s)\
    .voice_as_basic(True)
Quad.from_scale_step('i').set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_offset(9, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_scale_step('ii').set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_scale_step('v').set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_offset(10, dominant=True).set_fifth(6).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_offset(9, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_scale_step('ii').set_duration(2).add_to_sequence(s).voice_as_basic(True)
Quad.from_scale_step('v').set_duration(2).add_to_sequence(s).voice_as_basic(True)

s.play()

midi.close_outport()
