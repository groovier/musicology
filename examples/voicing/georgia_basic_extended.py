from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence
from musicology.extended import Extended
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

scale = Scale(5, SCALE_TYPES['major'])
s = Sequence(scale, 75)

Extended.from_offset(0, ninth='9').set_duration(4).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(11, minor=True, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(4, dominant=True, ninth='9').set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(9, minor=True, dominant=True, ninth='9').add_to_sequence(s).set_duration(2).voice_as_basic(True)
Extended.from_offset(0, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(5).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(6, minor=True, dominant=True).set_fifth(6).set_duration(2).add_to_sequence(s)\
    .voice_as_basic(True)
Extended.from_offset(0).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(9, dominant=True, ninth='9').set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(2, minor=True, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(7, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(10, dominant=True, ninth='9').set_fifth(6).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(9, dominant=True, ninth='9').set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(2, minor=True, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(7, dominant=True).set_duration(2).add_to_sequence(s).voice_as_basic(True)
Extended.from_offset(0, eleventh='#11', thirteenth='13').set_duration(4).add_to_sequence(s).voice_as_basic(True)

s.play()

midi.close_outport()
