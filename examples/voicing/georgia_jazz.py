from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence
from musicology.extended import Extended
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

scale = Scale(5, SCALE_TYPES['major'])
s = Sequence(scale, 75)

Extended.from_scale_step('i').set_duration(4).add_to_sequence(s).voice_as_jazz().play()
Extended.from_offset(11, minor=True, dominant=True).set_duration(2).add_to_sequence(s).voice_as_jazz().play()
Extended.from_offset(4, dominant=True).set_duration(2).add_to_sequence(s)\
    .voice_as_jazz(general_interval_options=(1, 3, 5, 7)).play()
Extended.from_scale_step('vi').set_duration(2).add_to_sequence(s)\
    .voice_as_jazz(general_interval_options=(1, 3, 5, 7, 9)).play()
Extended.from_offset(0, dominant=True).set_duration(2).add_to_sequence(s)\
    .voice_as_jazz(general_interval_options=(1, 3, 5, 7)).play()
Extended.from_scale_step('iv').set_duration(2).add_to_sequence(s).voice_as_jazz().play()
Extended.from_offset(6, minor=True, dominant=True).set_fifth(6).set_duration(2).add_to_sequence(s)\
    .voice_as_jazz(general_interval_options=(1, 3, 5, 7)).play()
Extended.from_scale_step('i').set_duration(2).add_to_sequence(s).voice_as_jazz().play()
Extended.from_offset(9, dominant=True).set_duration(2).add_to_sequence(s).voice_as_jazz().play()
Extended.from_scale_step('ii').set_duration(2).add_to_sequence(s).voice_as_jazz().play()
Extended.from_scale_step('v').set_duration(2).add_to_sequence(s).voice_as_jazz().play()
Extended.from_offset(10, dominant=True).set_fifth(6).set_duration(2).add_to_sequence(s).voice_as_jazz().play()
Extended.from_offset(9, dominant=True).set_duration(2).add_to_sequence(s).voice_as_jazz().play()
Extended.from_scale_step('ii').set_duration(2).add_to_sequence(s).voice_as_jazz().play()
Extended.from_scale_step('v').set_duration(2).add_to_sequence(s).voice_as_jazz().play()

# s.play()

midi.close_outport()
