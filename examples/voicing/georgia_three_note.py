from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence
from musicology.quad import Quad
from musicology.midi import MidiWrapper

scale = Scale(5, SCALE_TYPES['major'])
s = Sequence(scale, 75)
midi = MidiWrapper()
midi.open_outport()
# midi.new_file(tempo=sequence.tempo)


Quad.from_scale_step('i').set_duration(4).add_to_sequence(s).voice_as_137()
Quad.from_offset(11, minor=True, dominant=True).set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_offset(4, dominant=True).set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_scale_step('vi').set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_offset(0, dominant=True).set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_scale_step('iv').set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_offset(6, minor=True, dominant=True).set_fifth(6).set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_scale_step('i').set_duration(2).add_to_sequence(s).voice_as_157()
Quad.from_offset(9, dominant=True).set_duration(2).add_to_sequence(s).voice_as_173()
Quad.from_scale_step('ii').set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_scale_step('v').set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_offset(10, dominant=True).set_fifth(6).set_duration(2).add_to_sequence(s).voice_as_137()
Quad.from_offset(9, dominant=True).set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_offset(2, dominant=True).set_duration(2).add_to_sequence(s).voice_as_3_note()
Quad.from_scale_step('v').set_duration(2).add_to_sequence(s).voice_as_3_note()

# sequence.record()
# midi.save_file('georgia.mid')

s.play()
midi.close_outport()
