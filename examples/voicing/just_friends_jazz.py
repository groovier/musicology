from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence
from musicology.extended import Extended
from musicology.midi import MidiWrapper

scale = Scale(0, SCALE_TYPES['major'])
sequence = Sequence(scale, 90)
midi = MidiWrapper()
midi.open_outport()

chord01 = Extended.from_scale_step('i')
chord01.duration = 4
sequence.add_event(chord01)
chord01.voice_as_jazz().play()

chord02 = Extended.from_scale_step('i')
chord02.duration = 4
sequence.add_event(chord02)
chord02.voice_as_jazz().play()

chord03 = Extended.from_offset(0, minor=True, dominant=True)
chord03.duration = 4
sequence.add_event(chord03)
chord03.voice_as_jazz().play()

chord04 = Extended.from_offset(5, dominant=True)
chord04.duration = 4
sequence.add_event(chord04)
chord04.voice_as_jazz().play()

chord05 = Extended.from_offset(7)
chord05.duration = 4
sequence.add_event(chord05)
chord05.voice_as_jazz().play()

chord05 = Extended.from_offset(7,)
chord05.duration = 4
sequence.add_event(chord05)
chord05.voice_as_jazz().play()

chord06 = Extended.from_offset(10, minor=True, dominant=True)
chord06.duration = 4
sequence.add_event(chord06)
chord06.voice_as_jazz().play()

chord07 = Extended.from_offset(3, dominant=True)
chord07.duration = 4
sequence.add_event(chord07)
chord07.voice_as_jazz().play()

chord08 = Extended.from_scale_step('vi')
chord08.duration = 4
sequence.add_event(chord08)
chord08.voice_as_jazz().play()

chord09 = Extended.from_offset(2, dominant=True)
chord09.duration = 4
sequence.add_event(chord09)
chord09.voice_as_jazz().play()

chord10 = Extended.from_offset(7)
chord10.duration = 4
sequence.add_event(chord10)
chord10.voice_as_jazz().play()

chord11 = Extended.from_scale_step('iii')
chord11.duration = 4
sequence.add_event(chord11)
chord11.voice_as_jazz().play()

chord12 = Extended.from_offset(9, dominant=True)
chord12.duration = 4
sequence.add_event(chord12)
chord12.voice_as_jazz().play()

chord12 = Extended.from_offset(9, dominant=True)
chord12.duration = 4
sequence.add_event(chord12)
chord12.voice_as_jazz().play()

chord13 = Extended.from_offset(9, minor=True, dominant=True)
chord13.duration = 4
sequence.add_event(chord13)
chord13.voice_as_jazz().play()

chord14 = Extended.from_scale_step('ii')
chord14.duration = 2
sequence.add_event(chord14)
chord14.voice_as_jazz().play()

chord15 = Extended.from_scale_step('v')
chord15.duration = 2
sequence.add_event(chord15)
chord15.voice_as_jazz().play()

"""sequence.record()
midi_wrapper.save_file('georgia.mid')"""

sequence.play()
# sequence.record()
# midi_wrapper.save_file('just_friends.mid')
