from musicology.quad import Quad
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

quad_1 = Quad.from_scale_step('ii')
quad_1.voice_as_basic(True)
print(quad_1.voicing)
quad_1.play()

quad_2 = Quad.from_scale_step('v')
quad_2.voice_as_basic(True)
print(quad_2.voicing)
quad_2.play()

quad_3 = Quad.from_scale_step('i')
quad_3.voice_as_basic(True)
print(quad_3.voicing)
quad_3.play()

quad_4 = Quad.from_scale_step('iv')
quad_4.voice_as_basic(True)
print(quad_4.voicing)
quad_4.play()

midi.close_outport()
