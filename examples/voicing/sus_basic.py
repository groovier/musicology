from musicology.triad import Triad
from musicology.midi import MidiWrapper

midi = MidiWrapper()
midi.open_outport()

triad_1 = Triad(0, 5, 7)
triad_1.voice_as_basic()
print(triad_1.voicing)
triad_1.play()

triad_2 = Triad(0, 2, 7)
triad_2.voice_as_basic()
print(triad_2.voicing)
triad_2.play()

triad_3 = Triad(0, 4, 7)
triad_3.voice_as_basic()
print(triad_3.voicing)
triad_3.play()

midi.close_outport()
