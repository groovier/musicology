from musicology.note import Note

LOWEST_NOTE = Note.from_pitch_value(41)


class VoicingError(Exception):
    pass


class ChordNoteDoesNotExistError(Exception):
    pass
