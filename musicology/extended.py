from copy import deepcopy
from typing import Callable

from .quad import Quad
from .scale import ScaleType, SCALE_TYPES
from .note import Note
from .note_stack import NoteStack

from . import LOWEST_NOTE, VoicingError, ChordNoteDoesNotExistError

"""Addons are extensions that are not explicitly a part of the chord, but can be added in the voicing."""
ADDONS = {'b9': 1, '9': 2, '#9': 3, '11': 5, '#11': 6, 'b13': 8, '13': 9}


class Extended(Quad):
    """A chord with potentially seven notes: root, third, fifth, seventh, ninth, eleventh, and thirteenth.

    Normally, the last four notes (called the extensions) are NOT added at construction time unless they are specified
    in arguments. An exception from this is that when constructing the chord with the method from_scale_step, in fact
    ALL the extensions are created.
    """

    """ The values here are intervals represented by strings like 'b9' or '#11'. They can be translated into semitones
    by the ADDONS dict. The values are defined at construction time and can be used later for voicing the chord."""
    available_ninths = set()
    available_elevenths = set()
    available_thirteenths = set()

    def __init__(self, root: int, third: int, fifth: int, seventh: int,
                 ninth: int = None, eleventh: int = None, thirteenth: int = None):
        super().__init__(root, third, fifth, seventh)
        self.ninth = ninth
        self.eleventh = eleventh
        self.thirteenth = thirteenth
        if self.quality in ['major7', 'dominant7', 'minor7']:  # half-diminished is 'minor7'
            self.add_available_ninth_addons('9')
        if self.is_minor:
            self.add_available_elevenths_addons('11')
        if self.quality in ('major7', 'dominant7', 'minor7'):
            self.add_available_thirteenths_addons('13')
        if self.quality == 'major7':
            self.add_available_elevenths_addons('#11')
            self.add_available_thirteenths_addons('b13')

    def add_available_ninth_addons(self, *addons: str):
        if not self.ninth:
            for addon in addons:
                if addon in ('b9', '9', '#9'):
                    self.available_ninths.add(addon)

    def add_available_elevenths_addons(self, *addons: str):
        if not self.eleventh:
            for addon in addons:
                if addon in ('11', '#11'):
                    self.available_elevenths.add(addon)

    def add_available_thirteenths_addons(self, *addons: str):
        if not self.thirteenth:
            for addon in addons:
                if addon in ('b13', '13'):
                    self.available_thirteenths.add(addon)

    @property
    def quality(self):
        # Note: half-diminished is 'minor7'
        if self.is_major and not self.is_dominant:
            return 'major7'
        if self.is_major and self.is_dominant:
            return 'dominant7'
        if self.is_minor and self.is_dominant:
            return 'minor7'
        if self.is_minor and self.seventh == 9:
            return 'diminished'

    def ninth_pitch_class(self):
        return self.to_pitch_class(self.ninth)

    def eleventh_pitch_class(self):
        return self.to_pitch_class(self.eleventh)

    def thirteenth_pitch_class(self):
        return self.to_pitch_class(self.thirteenth)

    def __eq__(self, other):
        parent_check = super().__eq__(other)
        if not parent_check:
            return False
        if self.ninth == other.ninth and self.eleventh == other.eleventh and self.thirteenth == other.thirteenth:
            return True
        return False

    @classmethod
    def from_offset(cls, offset: int, minor: bool = False, ninth: str = None, eleventh: str = None,
                    thirteenth: str = None, dominant: bool = False,
                    base_scale_type: ScaleType = SCALE_TYPES['major']):
        """Build a Quad in a given 'offset' from the contextual root.

        :param offset: The chord roots' distance from the contextual root measured in semitones.
        :param minor: False if the chord is a major chord, otherwise True.
        :param base_scale_type: a ScaleType object.
        :param dominant: a Boolean that tells if the seventh should be low (True) og high (False).
        :param ninth: a string like 'b9' specifying the ninth type
        :param eleventh: a string like '#11' specifying the eleventh type
        :param thirteenth: a string containing either '13' or 'b13' to specify the thirteenth type
        """
        quad = super().from_offset(offset, minor, dominant, base_scale_type)

        if ninth is None:
            ninth_int = None
        elif ninth == 'b9':
            ninth_int = (quad.root + 1) % 12
        elif ninth == '9':
            ninth_int = (quad.root + 2) % 12
        elif ninth == '#9':
            ninth_int = (quad.root + 3) % 12
        else:
            raise ChordNoteDoesNotExistError(f"Illegal value for ninth: {ninth}.")

        if eleventh is None:
            eleventh_int = None
        elif eleventh == '11':
            eleventh_int = (quad.root + 5) % 12
        elif eleventh == '#11':
            eleventh_int = (quad.root + 6) % 12
        else:
            raise ChordNoteDoesNotExistError(f"Illegal value for eleventh: {eleventh}.")

        if thirteenth is None:
            thirteenth_int = None
        elif thirteenth == '13':
            thirteenth_int = (quad.root + 9) % 12
        elif thirteenth == 'b13':
            thirteenth_int = (quad.root + 8) % 12
        else:
            raise ChordNoteDoesNotExistError(f"Illegal value for thirteenth: {thirteenth}.")

        cls.set_scale_step(quad)
        return cls(quad.root, quad.third, quad.fifth, quad.seventh, ninth_int, eleventh_int, thirteenth_int)

    @classmethod
    def from_scale_step(cls, scale_step: str, base_scale_type: ScaleType = SCALE_TYPES['major']):
        """ Build an Extended with all possible extensions from a scale step in a diatonic scale.

        Warning: the resulting chord will actually contain all the notes in the scale, which makes it less useful unless
        you only use a subset of the notes in the voicing!

        :param scale_step: the step in the base scale where the triads' root is placed.
        :param base_scale_type: the ScaleType of the base scale.
        """
        chord_scale_type, chord_elements = cls.scale_step_to_chord_intervals(scale_step, base_scale_type)
        ext = cls(*chord_elements)
        ext.scale_step = scale_step
        ext.base_scale_type = base_scale_type
        ext.chord_scale_type = chord_scale_type
        return ext

    def set_ninth(self, ninth: int):
        if ninth not in [1, 2, 3]:
            raise ChordNoteDoesNotExistError(f"Illegal value for ninth: {ninth}.")
        self.ninth = (self.root + ninth) % 12
        return self

    def set_eleventh(self, eleventh: int):
        if eleventh not in [5, 6]:
            raise ChordNoteDoesNotExistError(f"Illegal value for eleventh: {eleventh}.")
        self.eleventh = (self.root + eleventh) % 12
        return self

    def set_thirteenth(self, thirteenth: int):
        if thirteenth != 9:
            raise ChordNoteDoesNotExistError(f"Illegal value for thirteenth: {thirteenth}.")
        self.thirteenth = (self.root + thirteenth) % 12
        return self

    def voice_as_basic(self, add_bass: bool = False):
        super().voice_as_basic(add_bass)
        if self.ninth:
            self.voicing.add_note(Note(self.ninth_pitch_class()))
        if self.eleventh:
            self.voicing.add_note(Note(self.eleventh_pitch_class()))
        if self.thirteenth:
            self.voicing.add_note(Note(self.thirteenth_pitch_class()))

    def voice_as_jazz(self,
                      number_of_voices: int = 5,
                      bottom_interval: int = 1,
                      second_interval_options: iter = (3, 7),
                      general_interval_options: iter = (1, 3, 5, 7, 9, 11, 13),
                      max_voice_movement: int = 5,
                      ):

        print("Voicing", self, "...")

        self.voicing = NoteStack()
        self.unused_in_voicing = [interval for interval in self.note_dict if interval is not None]

        # Add bottom note
        first_note = LOWEST_NOTE.stack((self.to_pitch_class(self.note_dict[bottom_interval]) % 12))
        self.voicing.add_note(first_note)
        self.unused_in_voicing.remove(bottom_interval)

        # Add second note
        self.add_to_voicing(second_interval_options, max_voice_movement)

        # Add the rest of the notes
        while len(self.voicing.notes) < number_of_voices:
            allowed = [i for i in self.unused_in_voicing if i in general_interval_options]
            try:
                self.add_to_voicing(allowed, max_voice_movement)
            except VoicingError:
                print("Normal voicing algorithm gave up. Use an octave.")
                self.voicing.stack(self.root_pitch_class)

        return self.voicing

    def add_to_voicing(self, interval_options, max_voice_movement):
        candidates = list()
        for interval in interval_options:
            candidate = self.voicing.top_note.stack(self.to_pitch_class(self.note_dict[interval]))

            # Check for 'avoid notes'
            # http://www.thejazzpianosite.com/jazz-piano-lessons/jazz-improvisation/avoid-notes/

            if candidate.pitch_class - self.root_pitch_class == 1:
                print("Avoid note 1 semitone over root avoided!")
                continue

            if candidate.pitch_class - self.third_pitch_class == 1:
                print("Avoid note 1 semitone over third avoided!")
                continue

            if candidate.pitch_class - self.fifth_pitch_class == 1:
                print("Avoid note 1 semitone over fifth avoided!")
                continue

            if candidate.pitch_class - self.seventh_pitch_class == 1:
                print("Avoid note 1 semitone over seventh avoided!")
                continue

            # Check voicing movement
            if self.has_previous_chord:
                same_voice_in_previous = self.previous.voicing.notes[len(self.voicing.notes)]
                pitch_difference = same_voice_in_previous.pitch_value - candidate.pitch_value
                if pitch_difference > max_voice_movement:
                    # print("Voice movement too large! It is", pitch_difference)
                    continue

            candidates.append((interval, candidate))

        if len(candidates) == 0:
            raise VoicingError(f"No voicing candidate notes found for {self} "
                               f"(already containing {len(self.voicing.notes)} voices).")

        try:
            note_tuple = min(candidate for candidate in candidates)
        except ValueError:
            note_tuple = candidates[0]
        self.voicing.add_note(note_tuple[1])
        self.unused_in_voicing.remove(note_tuple[0])

    @property
    def note_dict(self):
        """The chords' notes represented as a dictionary."""
        val = super().note_dict
        if self.ninth:
            val[9] = self.ninth
        if self.eleventh:
            val[11] = self.eleventh
        if self.thirteenth:
            val[13] = self.thirteenth
        return val
