from typing import Dict
from collections import deque

from musicology.scale import Scale
from musicology.triad import Triad
from musicology.extended import Extended


class ChordParsingError(Exception):
    pass


def note_name_to_pitch_class(note_name):
    if note_name == 'C':
        return 0
    if note_name in ('C#', 'Db'):
        return 1
    if note_name == 'D':
        return 2
    if note_name in ('D#', 'Eb'):
        return 3
    if note_name == 'E':
        return 4
    if note_name == 'F':
        return 5
    if note_name in ('F#', 'Gb'):
        return 6
    if note_name == 'G':
        return 7
    if note_name in ('G#', 'Ab'):
        return 8
    if note_name == 'A':
        return 9
    if note_name in ('A#', 'Bb'):
        return 10
    if note_name == 'B':
        return 11
    return ChordParsingError(f"Illegal note name: {note_name}.")


def create_structure_from_dict(chord_dict: Dict, scale: Scale):
    # In order to use Musicology optimally, we should first relativize the chord.
    offset_calculator = deque([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
    offset_calculator.rotate(scale.tonic)
    root_note_pitch_class = note_name_to_pitch_class(chord_dict['normalized']['rootNote'])
    offset = offset_calculator[root_note_pitch_class]
    quality = chord_dict['normalized']['quality']
    extensions = chord_dict['normalized']['extensions']
    alterations = chord_dict['normalized']['alterations']

    if (len(extensions) > 0) or (quality[-1] == '7'):
        klass = Extended
    else:
        klass = Triad

    # Parse chord quality.
    if quality == 'major':
        structure = klass.from_offset(
            offset=offset,
            minor=False,
            base_scale_type=scale.scale_type)

    elif quality == 'minor':
        structure = klass.from_offset(
            offset=offset,
            minor=True,
            base_scale_type=scale.scale_type)

    elif quality == 'diminished':
        structure = klass.from_offset(
            offset=offset,
            minor=True,
            base_scale_type=scale.scale_type)
        structure.set_fifth(6)

    elif quality == 'augmented':
        structure = klass.from_offset(
            offset=offset,
            minor=False,
            base_scale_type=scale.scale_type)
        structure.set_fifth(8)

    elif quality == 'major7':
        structure = klass.from_offset(
            offset=offset,
            minor=False,
            base_scale_type=scale.scale_type)

    elif quality == 'minor7':
        structure = klass.from_offset(
            offset=offset,
            minor=True,
            dominant=True,
            base_scale_type=scale.scale_type)

    elif quality == 'minorMajor7':
        structure = klass.from_offset(
            offset=offset,
            minor=True,
            dominant=False,
            base_scale_type=scale.scale_type)

    elif quality == 'dominant7':
        structure = klass.from_offset(
            offset=offset,
            minor=False,
            dominant=True,
            base_scale_type=scale.scale_type)

    elif quality == 'diminished7':
        structure = klass.from_offset(
            offset=offset,
            minor=True,
            dominant=True,
            base_scale_type=scale.scale_type)
        structure.set_fifth(6)
        structure.set_seventh(9)

    else:
        raise ChordParsingError(f"Illegal value for chord quality: {quality}.")

    structure.base_scale_pitch_class = scale.tonic

    # Parse chord extensions.
    if '9' in extensions:
        structure.set_ninth(2)

    if '11' in extensions:
        structure.set_eleventh(5)

    if '13' in extensions:
        structure.set_thirteenth(9)

    # Parse chord alterations
    if 'b5' in alterations:
        structure.set_fifth(6)

    if '#5' in alterations:
        structure.set_fifth(8)

    if 'b9' in alterations:
        structure.set_ninth(1)

    if '#9' in alterations:
        structure.set_ninth(3)

    if '#11' in extensions:
        structure.set_eleventh(6)

    if 'b13' in extensions:
        structure.set_thirteenth(8)

    # Sus4 chords
    if chord_dict['normalized']['isSuspended']:
        structure.set_third(5)

    return structure
