import mido


class Singleton(type):
    """
    https://michaelgoerz.net/notes/singleton-objects-in-python.html
    Metaclass for singletons. Any instantiation of a Singleton class yields
    the exact same object, e.g.:

    class MyClass(metaclass=Singleton):
        pass
    a = MyClass()
    b = MyClass()
    a is b
    True
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

    @classmethod
    def __instancecheck__(mcs, instance):
        if instance.__class__ is mcs:
            return True
        else:
            return isinstance(instance.__class__, mcs)


class MidiWrapper(metaclass=Singleton):

    outport: None
    midi_file: None
    track: None
    ticks_per_beat: int

    def open_outport(self):
        self.outport = mido.open_output('Microsoft GS Wavetable Synth 0')

    def close_outport(self):
        self.outport.close()

    def new_file(self, tempo: int, key_signature: str = 'C'):
        self.ticks_per_beat = 800
        self.midi_file = mido.MidiFile(ticks_per_beat=self.ticks_per_beat)
        self.track = mido.MidiTrack()
        self.track.append(mido.MetaMessage('set_tempo', tempo=round(60 / tempo * 1000000)))
        self.track.append(mido.MetaMessage(type='key_signature', key=key_signature))
        self.midi_file.tracks.append(self.track)

    def save_file(self, file_name: str):
        self.track.append(mido.MetaMessage('end_of_track'))
        self.midi_file.save(file_name)

    def note_on(self, midi_note: int, velocity: int = 100):
        self.outport.send(mido.Message('note_on', note=midi_note, velocity=velocity))

    def note_off(self, midi_note: int):
        self.outport.send(mido.Message('note_off', note=midi_note))



