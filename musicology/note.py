from .playable import Playable

WHITE_KEYS = {
    0: 'C',
    2: 'D',
    4: 'E',
    5: 'F',
    7: 'G',
    9: 'A',
    11: 'B'
}


def transp(original_step: int, interval: int, original_octave: int):
    new_step = original_step
    new_octave = original_octave
    i = 0
    if interval > 0:
        while i < interval:
            if new_step == 11:
                new_step = 0
                new_octave += 1
            else:
                new_step += 1
            i += 1
    if interval < 0:
        while i > interval:
            if new_step == 0:
                new_step = 11
                new_octave -= 1
            else:
                new_step -= 1
            i -= 1
    return new_step, new_octave


class Note(Playable):
    """A class representing a basic conceptualization of a musical note.
    
    Arguments to the default constructor:

    - pitch_class: a positive integer representing the notes' placement in a chromatic scale starting from c where 0
    represents a 'c' note, 1 represents a 'c sharp' or a 'd flat' note, etc. A value of 11 will thus represent a 'b'
    note.
    
    - octave: an optional integer indicating an octave. Octave numbering is done in the same way as in 'scientific pitch
    notation' (https://en.wikipedia.org/wiki/Scientific_pitch_notation), i. e. the 'middle c' (MIDI  note number 60) is
    regarded as the start of octave number 4. 4 is the default value for octave.
    """
    
    def __init__(self, pitch_class: int, octave: int = 4):
        super().__init__()
        if pitch_class not in range(12):
            raise ValueError(
                "pitch_class must be an integer from 0 a to 11, both included."
                )
        self.octave = octave
        self.pitch_class = pitch_class

    @classmethod
    def from_pitch_value(cls, pitch_value: int):
        midi_octave = pitch_value // 12
        pitch_class = pitch_value % 12
        return cls(pitch_class, midi_octave - 1)

    def transpose(self, interval: int):
        (self.pitch_class, self.octave) = transp(self.pitch_class, interval, self.octave)

    def transpose_as_new(self, interval: int):
        new_step, new_octave = transp(self.pitch_class, interval, self.octave)
        return Note(new_step, new_octave)

    def stack(self, stack_value: int, accept_identical: bool = False, large_interval: bool = False):
        """
        Stack a new note on top of the note (self).
        :param stack_value: stack_value is a combination of a pitch class and an octave delta (number of octaves the new
        note must be transposed). The actual values for pitch_class and octave_delta are carried out like this:
        - pitch_class = stack_value % 12
        - octave_delta = stack_value // 12

        :param accept_identical: if a note with the same pitch as the current note should be accepted.
        :param large_interval: if True, octave_delta will be added to the new Notes' octave. I False, octave_delta will
        be ignored.
        :return: the new Note.
        """
        pitch_class = stack_value % 12
        octave_delta = stack_value // 12
        new_note = Note(pitch_class, 0)
        if accept_identical:
            while new_note.pitch_value < self.pitch_value:
                new_note.octave += 1
        else:
            while new_note.pitch_value <= self.pitch_value:
                new_note.octave += 1
        if large_interval:
            new_note.octave += octave_delta
        return new_note
    
    @property
    def white_key(self):
        return WHITE_KEYS.get(self.pitch_class)
    
    @property
    def as_sharp(self):
        return self.white_key or WHITE_KEYS[self.pitch_class - 1] + '#'
    
    @property
    def as_flat(self):
        return self.white_key or WHITE_KEYS[self.pitch_class + 1] + 'b'
    
    @property
    def midi_octave(self):
        return self.octave + 1

    @property
    def pitch_value(self):
        return self.midi_octave * 12 + self.pitch_class
    
    def __repr__(self):
        return self.white_key or f"{self.as_sharp}/{self.as_flat}"
    
    def __str__(self):
        return f"Name: {self.__repr__()}, Octave: {self.octave}, MIDI note: {self.pitch_value}"

    @property
    def notes(self):
        return [self]

    def __gt__(self, other):
        return self.pitch_value > other.pitch_value
