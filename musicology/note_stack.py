from typing import List

from .playable import Playable
from .note import Note


class NoteStack(Playable):
    """A 'low level chord' -  simply a stack of notes without knowledge of their harmonic relations."""

    _movement: int = None

    def __init__(self, *notes: Note):
        super().__init__()
        self._notes = {}
        for note in notes:
            self.add_note(note)

    @classmethod
    def from_pitch_values(cls, *pitch_values: int):
        notes = [Note.from_pitch_value(pitch_value) for pitch_value in pitch_values]
        return cls(*notes)
    
    def add_note(self, note):
        try:
            self._notes[note.pitch_value] = note
        except KeyError:
            pass
    
    def __repr__(self):
        sorted_keys = sorted(self._notes)
        return str([self._notes[key] for key in sorted_keys])
    
    def __str__(self):
        sorted_keys = sorted(self._notes)
        return str([self._notes[key].__str__() for key in sorted_keys])

    @property
    def notes(self):
        return sorted(list(self._notes.values()), key=lambda note: note.pitch_value)

    @property
    def pitch_values(self):
        return sorted(list(self._notes.keys()))

    @property
    def movement(self):
        return self._movement

    @property
    def top_note(self):
        return max(self.notes, key=lambda x: x.pitch_value)

    def stack(self, pitch_class, accept_identical: bool = False, large_interval: bool = False):
        new_note = self.top_note.stack(pitch_class, accept_identical, large_interval)
        self.add_note(new_note)
        return self

    def calculate_movement(self, previous_notes: List[Note]):
        if len(previous_notes) != len(self.notes):
            raise ValueError("You can only calculate voice movement between two chords with the same number of voices.")
        up = down = voice_number = 0
        while voice_number < len(self.notes):
            movement = self.notes[voice_number].pitch_value - previous_notes[voice_number].pitch_value
            if movement > 0:
                up += movement
            if movement < 0:
                down += movement * -1
            voice_number += 1
        self._movement = up + down
