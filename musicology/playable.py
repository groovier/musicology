from abc import ABC, abstractmethod
from time import sleep

import mido

from .midi import MidiWrapper

midi = MidiWrapper()


class Playable(ABC):
    """ A basic musical entity with a duration and one or more notes that should be playing in the entire duration."""

    def __init__(self):
        # A duration of 1 equals a fourth note.
        self.duration = 2.0
        # tempo is in beats per minute.
        self.tempo = 90.0
        self.base_scale_type = None

        """ See https: // en.wikipedia.org / wiki / Pitch_class for a definition."""
        self.base_scale_pitch_class = 0

    def set_duration(self, duration: int):
        """Convenience method that sets the duration and returns the object."""
        self.duration = duration
        return self

    @property
    def play_time(self):
        return 60 / self.tempo * self.duration

    @property
    @abstractmethod
    def notes(self):
        """This method must always return a list of Note objects."""
        pass

    def play(self):
        print(self.notes)
        for note in self.notes:
            midi.note_on(note.pitch_value)
        sleep(self.play_time)
        for note in self.notes:
            midi.note_off(note.pitch_value)

    def record(self):
        for note in self.notes:
            midi.track.append(mido.Message('note_on', note=note.pitch_value, time=0, velocity=100))
        midi.track.append(mido.Message('note_off', time=int(midi.ticks_per_beat * self.tempo / 60 * self.play_time)))
        for note in self.notes:
            midi.track.append(mido.Message('note_off', note=note.pitch_value))
