from copy import deepcopy
from typing import Callable

from musicology.triad import Triad
from musicology.scale import ScaleType, SCALE_TYPES
from musicology.note import Note
from musicology.note_stack import NoteStack

from musicology import LOWEST_NOTE, VoicingError


class Quad(Triad):
    """A chord consisting of four notes: root, third, fifth and seventh."""
    def __init__(self, root: int, third: int, fifth: int, seventh: int):
        super().__init__(root, third, fifth)
        self.seventh = seventh

    @property
    def seventh_pitch_class(self):
        return self.to_pitch_class(self.seventh)

    def __eq__(self, other):
        parent_check = super().__eq__(other)
        if not parent_check:
            return False
        if self.seventh == other.seventh:
            return True
        return False

    @classmethod
    def from_offset(cls, offset: int, minor: bool = False, dominant: bool = False,
                    base_scale_type: ScaleType = SCALE_TYPES['major']):
        """Build a Quad in a given 'offset' from the contextual root.

        :param offset: The chord roots' distance from the contextual root measured in semitones.
        :param minor: False if the chord is a major chord, otherwise True.
        :param base_scale_type: a ScaleType object.
        :param dominant: a Boolean that tells if the seventh should be low (True) og high (False).
        """

        triad = Triad.from_offset(offset, minor, base_scale_type or None)
        seventh = (offset + 10 if dominant else offset + 11) % 12
        quad = cls(triad.root, triad.third, triad.fifth, seventh)
        quad.base_scale_type = base_scale_type
        cls.set_scale_step(quad)
        return quad

    @classmethod
    def from_scale_step(cls, scale_step: str, base_scale_type: ScaleType = SCALE_TYPES['major']):
        """ Build a Quad from a scale step in a diatonic scale.

        :param scale_step: the step in the base scale where the triads' root is placed.
        :param base_scale_type: the ScaleType of the base scale.
        """
        chord_scale_type, chord_elements = cls.scale_step_to_chord_intervals(scale_step, base_scale_type)
        quad = cls(*chord_elements[:4])
        quad.scale_step = scale_step
        quad.base_scale_type = base_scale_type
        quad.chord_scale_type = chord_scale_type
        return quad

    @property
    def note_dict(self):
        """The cords' notes represented as a dictionary."""
        val = super().note_dict
        val[7] = self.seventh
        return val

    @property
    def is_dominant(self):
        if (self.seventh - self.root) % 12 == 10:
            return True
        return False

    def set_seventh(self, seventh: int):
        if seventh not in [9, 10, 11]:
            raise ValueError(f"{seventh} is not an acceptable value for seventh.")
        self.seventh = (self.root + seventh) % 12
        return self

    def voice_as_basic(self, add_bass: bool = False):
        super().voice_as_basic(add_bass)
        self.voicing.add_note(Note(self.seventh_pitch_class))

    def voice_as_137(self):
        self.voicing = NoteStack(LOWEST_NOTE.stack(self.root_pitch_class))
        self.voicing.stack(self.third_pitch_class)
        self.voicing.stack(self.seventh_pitch_class)
        return self.voicing

    def voice_as_173(self):
        self.voicing = NoteStack(LOWEST_NOTE.stack(self.root_pitch_class))
        self.voicing.stack(self.seventh_pitch_class)
        self.voicing.stack(self.third_pitch_class)
        return self.voicing

    def voice_as_157(self):
        self.voicing = NoteStack(LOWEST_NOTE.stack(self.root_pitch_class))
        self.voicing.stack(self.fifth_pitch_class)
        self.voicing.stack(self.seventh_pitch_class)
        return self.voicing

    def voice_with_least_movement(self, *voicing_methods: Callable):
        """Tries several voicing methods and chooses the one that results in the least voice movement.
        :param voicing_methods: the voicing methods to try.
        """
        if not self.has_previous_chord:
            raise VoicingError("Cannot voice with least movement as there is no previous chord.")
        alternatives = []
        for method in voicing_methods:
            alternatives.append(method())
        for alternative in alternatives:
            alternative.calculate_movement(self.previous.voicing.notes)
        self.voicing = min(alternatives, key=lambda x: x.movement)

    def stack_with_least_movement(self, pitch_class: int, *intervals: int):
        """Tries to stack different notes on the chord and chooses the one that results in the least voice movement.
        :param pitch_class: pitch class ('key') for the voicing.
        :param intervals: the intervals to try (f. ex. 2 for a ninth or 6 for a #11).
        """
        if not self.has_previous_chord:
            raise VoicingError("Cannot stack with least movement as there is no previous chord.")
        alternatives = []
        for interval in intervals:
            chord = deepcopy(self.voicing)
            chord.stack(pitch_class, interval)
            alternatives.append(chord)
        for alternative in alternatives:
            alternative.calculate_movement(self.previous.voicing.notes)
        self.voicing = min(alternatives, key=lambda x: x.movement)

    def voice_as_3_note(self):
        self.voice_with_least_movement(self.voice_as_137, self.voice_as_173)
