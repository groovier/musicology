from typing import List
from collections import deque

from .note import Note


class ScaleType:
    def __init__(self, name: str, steps: List[int],  key_signature_function: callable = None):
        self.name = name
        self.key_signature_function = key_signature_function
        self.steps = deque()
        for step in steps:
            if step not in range(12):
                raise ValueError(f"Can not add step {step}. step must be an integer between 0 and 11, both included.")
            if len(self.steps) == 0 and step != 0:
                raise ValueError("The first step in a ScaleType must always be 0.")
            if len(self.steps) > 0 and not step > self.steps[-1]:
                raise ValueError("Each step in a ScaleType must be above the previous step.")
            self.steps.append(step)
    
    def __repr__(self):
        return ' '.join([str(step) for step in self.steps])

    def __str__(self):
        return self.name

    def rotate(self, n):
        self.name = "diatonic (unknown)"
        self.steps.rotate(n)

    def get_interval(self, scale_step):
        """Returns the chromatic interval between the scales' tonic and scale_step.
        We cannot simply ask for scale_type.steps[scale_step] since scale_step might be a value above the highest
        scale step defined i ScaleType.step."""
        scale_length = len(self.steps)
        if scale_step >= scale_length:
            number_of_octaves_to_transpose = scale_step // 12
            modulus_step = scale_step % scale_length
            return 12 * number_of_octaves_to_transpose + self.steps[modulus_step]
        else:
            return self.steps[scale_step]


def major_key_signature(step):
    base_key_signatures = {
        0: 0,
        2: 2,
        4: 4,
        5: -1,
        7: 1,
        9: 3,
        11: 5
    }
    key_signature = base_key_signatures.get(step)
    next_step = None
    if key_signature is None:
        next_step = base_key_signatures[step + 1]
        if next_step is not None:
            key_signature = next_step - 7
    if key_signature is None:
        previous_step = base_key_signatures[step - 1]
        if next_step is not None:
            key_signature = previous_step + 7
    return key_signature


def minor_key_signature(step):
    return major_key_signature(step) - 3


SCALE_TYPES = {
    'chromatic': ScaleType("chromatic", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], lambda pitch_class: 0),
    'major': ScaleType("major", [0, 2, 4, 5, 7, 9, 11], major_key_signature),
    'harmonic_minor': ScaleType("harmonic minor", [0, 2, 3, 5, 7, 8, 11], minor_key_signature),
}


class Scale:
    def __init__(self, tonic: int, scale_type: ScaleType):
        if tonic not in range(12):
            raise ValueError("tonic must be an integer between 0 and 11, both included.")
        self.tonic = tonic
        self.scale_type = scale_type
        self.notes = []
        octave = 4
        for step in self.scale_type.steps:
            pitch_class = tonic + step
            if pitch_class > 11:
                octave = 5
                pitch_class -= 12
            self.notes.append(Note(pitch_class, octave))
        self.notes.append(Note(tonic, octave + 1 if tonic == 0 else octave))

    @property
    def key_signature_numeric(self):
        if self.scale_type.key_signature_function:
            return self.scale_type.key_signature_function(self.notes[0].pitch_class)
        else:
            return None

    @property
    def key_signature(self):
        note_name = (self.notes[0].__repr__())
        if len(note_name) == 1:
            signature = note_name
        else:
            (sharp, flat) = note_name.split('/')
            signature = sharp if self.key_signature_numeric > 0 else flat
        if self.scale_type is not SCALE_TYPES['major']:
            # Assume that it's a minor scale.
            signature += 'm'
        return signature
    
    def __repr__(self):
        return f"{self.notes[0].__repr__()} {self.scale_type.name}"
    
    def __str__(self):
        line = f"{self.__repr__()} (key sign. {self.key_signature_numeric}):"
        for note in self.notes:
            line += f" {note.as_flat if self.key_signature_numeric < 0 else note.as_sharp}"
        return line

