from typing import Tuple

from .scale import Scale
from .note import Note
from .playable import Playable


class Frame(Playable):
    """Frame is a basic note container."""
    def __init__(self):
        super().__init__()
        self._notes = []

    @property
    def notes(self):
        return self._notes

    def add_note(self, note: Note):
        self._notes.append(note)

    @classmethod
    def from_chromatic_steps(cls, *chromatic_steps: Tuple[int, int]):
        """chromatic_steps must be one or more tuples of (pitch_class, octave)."""
        frame = cls()
        for chrom in chromatic_steps:
            frame.add_note(Note(*chrom))
        return frame

    @classmethod
    def from_scale_steps(cls, scale: Scale, *scale_steps: Tuple[int, int]):
        base_note = scale.notes[0]
        frame = cls()
        for scale_step in scale_steps:
            transpose_interval = scale.scale_type.get_interval(scale_step[0])
            note = base_note.transpose_as_new(transpose_interval)
            note.octave = scale_step[1]
            frame.add_note(note)
        return frame


class Sequence:
    def __init__(self, scale: Scale, tempo: float = 120):
        self.scale = scale
        self._tempo = tempo
        self._events = []

    def __repr__(self):
        lines = []
        for event in self._events:
            lines.append([note.as_flat if self.scale.key_signature_numeric < 0 else note.as_sharp for note in event.notes])
        return str(lines)

    @property
    def events(self):
        return self._events

    def add_event(self, event: Playable):
        if len(self._events) > 0:
            event.previous = self._events[-1]
        else:
            event.previous = None
        event.tempo = self._tempo
        event.base_scale_pitch_class = self.scale.tonic
        event.base_scale_type = self.scale.scale_type
        self._events.append(event)


    @property
    def tempo(self):
        return self._tempo

    @tempo.setter
    def tempo(self, tempo):
        self._tempo = tempo
        for event in self.events:
            event.tempo = tempo

    def play(self):
        for event in self._events:
            event.play()

    def record(self):
        for event in self._events:
            event.record()
