from copy import deepcopy

from musicology import LOWEST_NOTE, ChordNoteDoesNotExistError
from musicology.scale import ScaleType, SCALE_TYPES
from musicology.note import Note
from musicology.note_stack import NoteStack
from musicology.playable import Playable
from musicology.sequence import Sequence


def integer_to_roman(integer: int):
    """Convert integers from 1 to 7 to Roman numbering."""
    if integer == 1:
        return 'i'
    if integer == 2:
        return 'ii'
    if integer == 3:
        return 'iii'
    if integer == 4:
        return 'iv'
    if integer == 5:
        return 'v'
    if integer == 6:
        return 'vi'
    if integer == 7:
        return 'vii'
    raise ValueError('This function only supports integers from 1 to 7.')


class Triad(Playable):
    """A chord consisting of three notes: root, third and fifth.

    The three notes are represented by integers defining the notes' distance in semitones to the 'contextual root',
    which is defined in the base class, Playable, as self.base_scale_pitch_class'.
    """

    def __init__(self, root: int, third: int, fifth: int):
        super().__init__()
        self.root = root
        self.third = third
        self.fifth = fifth
        self.voicing = None
        self.previous = None
        self.scale_step = None
        self.chord_scale_type = None
        self.lower_limit = deepcopy(LOWEST_NOTE)
        self.unused_in_voicing = None

    def to_pitch_class(self, interval: int):
        """Convert an interval (relative to self.base_scale_pitch_class) to a pitch class."""
        return (self.base_scale_pitch_class + interval) % 12

    @property
    def root_pitch_class(self):
        return self.to_pitch_class(self.root)

    @property
    def third_pitch_class(self):
        return self.to_pitch_class(self.third)

    @property
    def fifth_pitch_class(self):
        return self.to_pitch_class(self.fifth)

    def __str__(self):
        root_pitch_class = (self.root + self.base_scale_pitch_class) % 12
        simple_symbol = ''
        if root_pitch_class == 0:
            simple_symbol = 'C'
        if root_pitch_class == 1:
            simple_symbol = 'C#/Db'
        if root_pitch_class == 2:
            simple_symbol = 'D'
        if root_pitch_class == 3:
            simple_symbol = 'D#/Eb'
        if root_pitch_class == 4:
            simple_symbol = 'E'
        if root_pitch_class == 5:
            simple_symbol = 'F'
        if root_pitch_class == 6:
            simple_symbol = 'F#/Gb'
        if root_pitch_class == 7:
            simple_symbol = 'G'
        if root_pitch_class == 8:
            simple_symbol = 'G#/Ab'
        if root_pitch_class == 9:
            simple_symbol = 'A'
        if root_pitch_class == 10:
            simple_symbol = 'A#/Bb'
        if root_pitch_class == 11:
            simple_symbol = 'B'
        if self.is_minor:
            simple_symbol += 'm'
        return f"{simple_symbol}'ish chord"

    def __eq__(self, other):
        if type(self) != type(other):
            return False
        if self.root == other.root and self.third == other.third and self.fifth == other.fifth:
            return True
        return False

    @classmethod
    def set_scale_step(cls, structure):
        """If possible, define what scale step in the tonal context the chord is built on."""

        if structure.root in structure.base_scale_type.steps:
            scale_step_integer = structure.base_scale_type.steps.index(structure.root)
            scale_step = integer_to_roman(scale_step_integer + 1)  # Add 1 because ScaleType.steps is zero-based.
            scale_private_from_same_step = cls.from_scale_step(scale_step, structure.base_scale_type)
            if scale_private_from_same_step == structure:
                structure.scale_step = scale_step

    @staticmethod
    def scale_step_to_chord_intervals(scale_step: str, base_scale_type: ScaleType = SCALE_TYPES['major']):
        """Build chord intervals from a given scale step.

        :param scale_step: the scale step from where to build the chord.
        :param base_scale_type: Defaults to the Major scale type (maybe this is in fact the only use case).
        :return: A tuple consisting of:
            - The 'rotated' scale (normally some diatonic scale)
            - A list af 7 integers representing the 'fully extended chord' that can be built from this scale step
        """
        chord_scale_type = deepcopy(base_scale_type)  # Create a new ScaleType object so we don't mess up the original
        if scale_step == 'ii':
            chord_scale_type.rotate(-1)
        elif scale_step == 'iii':
            chord_scale_type.rotate(-2)
        elif scale_step == 'iv':
            chord_scale_type.rotate(-3)
        elif scale_step == 'v':
            chord_scale_type.rotate(-4)
        elif scale_step == 'vi':
            chord_scale_type.rotate(-5)
        elif scale_step == 'vii':
            chord_scale_type.rotate(-6)
        else:
            if scale_step != 'i':
                raise ChordNoteDoesNotExistError(f"Illegal value for scale step: {scale_step}")
        root = chord_scale_type.steps[0]
        third = chord_scale_type.steps[2]
        fifth = chord_scale_type.steps[4]
        seventh = chord_scale_type.steps[6]
        ninth = chord_scale_type.steps[1]
        eleventh = chord_scale_type.steps[3]
        thirteenth = chord_scale_type.steps[5]
        return chord_scale_type, [root, third, fifth, seventh, ninth, eleventh, thirteenth]

    @classmethod
    def from_offset(cls, offset: int, minor: bool = False, base_scale_type: ScaleType = SCALE_TYPES['major']):
        """Build a Triad in a given 'offset' from the contextual root. This is the way a chord would normally be
        constructed.

        :param offset: The chord roots' distance from the contextual root measured in semitones.
        :param minor: False if the chord is a major chord, otherwise True.
        :param base_scale_type: a ScaleType object.
        """

        root = offset
        third = (offset + 3 if minor else offset + 4) % 12
        fifth = (offset + 7) % 12
        triad = cls(root, third, fifth)
        triad.base_scale_type = base_scale_type
        cls.set_scale_step(triad)
        return triad

    @classmethod
    def from_scale_step(cls, scale_step: str, base_scale_type: ScaleType = SCALE_TYPES['major']):
        """ Build a Triad from a scale step in a diatonic scale.

        :param scale_step: the step in the base scale where the triads' root is placed.
        :param base_scale_type: the ScaleType of the base scale.
        """

        chord_scale_type, chord_elements = cls.scale_step_to_chord_intervals(scale_step, base_scale_type)
        triad = cls(*chord_elements[:3])
        triad.scale_step = scale_step
        triad.base_scale_type = base_scale_type
        triad.chord_scale_type = chord_scale_type
        return triad

    @property
    def note_dict(self):
        """The cords' notes represented as a dictionary."""

        return {
            1: self.root,
            3: self.third,
            5: self.fifth
        }

    @property
    def is_minor(self):
        """The definition of 'minor' is that the third is the minor third."""

        if self.third - self.root in [3, -9]:
            return True
        return False

    @property
    def is_major(self):
        """The definition of 'major' is that the third is the major third."""

        if self.third - self.root in [4, -8]:
            return True
        return False

    def set_third(self, third: int):
        """Set the interval for the third in semitones.

        2: Replace third with a large second.
        3: Minor third.
        4: Major third.
        5: Replace the third with a fourth.
        """

        if third not in [2, 3, 4, 5]:
            raise ValueError(f"{third} is not an acceptable value for third.")
        self.third = (self.root + third) % 12
        return self

    def set_fifth(self, fifth: int):
        """Set the fifth: 1 if the fifth is high, 0 if perfect, -1 if low."""

        if fifth not in [6, 7, 8]:
            raise ValueError(f"{fifth} is not an acceptable value for fifth.")
        self.fifth = (self.root + fifth) % 12
        return self

    def voice_as_basic(self, add_bass: bool = False):
        """Create a basic 'voicing' of the chord in self.voicing.

        :param add_bass: add an extra root note one octave below the normal root note.
        """

        self.voicing = NoteStack(
            Note(self.root_pitch_class),
            Note(self.third_pitch_class),
            Note(self.fifth_pitch_class)
        )
        if add_bass:
            self.voicing.add_note(Note(self.root_pitch_class, 3))

    @property
    def notes(self):
        return self.voicing.notes if hasattr(self.voicing, 'notes') else []

    @property
    def pitch_values(self):
        return self.voicing.pitch_values if hasattr(self.voicing, 'pitch_values') else []

    @property
    def has_previous_chord(self):
        if self.previous and hasattr(self.previous, 'voicing'):
            return True
        return False

    def add_to_sequence(self, sequence: Sequence):
        sequence.add_event(self)
        return self
