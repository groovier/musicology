from musicology.note import Note
from musicology.note_stack import NoteStack


def test_chord():
    root = Note(0)
    third = root.transpose_as_new(4)
    fifth = third.transpose_as_new(3)
    chord = NoteStack(root, fifth, third, root)
    print(chord)
    print(chord.notes)
    assert 'C' in (str(chord.notes))
    assert 'E' in (str(chord.notes))
    assert 'G' in (str(chord.notes))
