from musicology.extended import Extended


def test_ext_is_dominant():
    ext = Extended(0, 4, 7, 10)
    assert ext.is_dominant


def test_ext_is_not_dominant():
    ext = Extended(0, 4, 7, 11)
    assert not ext.is_dominant


