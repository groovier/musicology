import json

from musicology.scale import Scale, SCALE_TYPES
from musicology.extended import Extended
from musicology.factories import create_structure_from_dict


def test_create_structure_from_dict():
    scale = Scale(0, SCALE_TYPES['major'])
    cmaj7_json = '{"input": {"symbol": "Cmaj7", "rootNote": "C", "descriptor": "maj7", "parsableDescriptor": "maj7"},' \
                 '"formatted": {"rootNote": "C", "descriptor": "ma7", "chordChanges": []},' \
                 '"normalized": {"adds": [], "omits": [],' \
                 '"intents": {"major": true, "eleventh": false}, "quality": "major7", "rootNote": "C",' \
                 '"intervals": ["1", "3", "5", "7"], "semitones": [0, 4, 7, 11], "extensions": [],' \
                 '"alterations": [], "isSuspended": false}}'
    cmaj7_dict = json.loads(cmaj7_json)
    structure = create_structure_from_dict(cmaj7_dict, scale)
    assert structure.root == 0
    assert structure.third == 4
    assert structure.fifth == 7
    assert structure.seventh == 11

