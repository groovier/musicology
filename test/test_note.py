from musicology.note import Note


def test_notes():
    notes = []
    root = Note(0)
    notes.append(root)
    third = root.transpose_as_new(4)
    notes.append(third)
    fifth = third.transpose_as_new(3)
    notes.append(fifth)
    seventh = fifth.transpose_as_new(3)
    notes.append(seventh)
    print(notes)
    for note in notes:
        print(note.__str__())
    assert root.pitch_value == 60
    assert third.pitch_value == 64
    assert fifth.pitch_value == 67
    assert seventh.pitch_value == 70
    for note in notes:
        note.transpose(5)
    print(notes)
    for note in notes:
        print(note.__str__())
    assert root.pitch_value == 65
    assert third.pitch_value == 69
    assert fifth.pitch_value == 72
    assert seventh.pitch_value == 75


def test_stacking():
    a = Note(9)
    d = a.stack(2)
    assert d.pitch_value > a.pitch_value
