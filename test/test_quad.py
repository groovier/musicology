from musicology.quad import Quad


def test_quad_is_dominant():
    quad = Quad(0, 4, 7, 10)
    assert quad.is_dominant


def test_quad_is_not_dominant():
    quad = Quad(0, 4, 7, 11)
    assert not quad.is_dominant


def test_scale_step_when_offset_private():
    """Build a scale private Quad with from_offset and assert that it gets the correct scale_step."""
    quad = Quad.from_offset(2, minor=True, dominant=True)
    assert quad.scale_step == 'ii'


def test_scale_step_when_offset_foreign():
    """Build a scale foreign Quad with from_offset and assert that its scale_step is None."""
    quad = Quad.from_offset(2, minor=False, dominant=True)
    assert quad.scale_step is None

