from musicology.scale import Scale, SCALE_TYPES


def test_f_major_scale():
    f_major = Scale(5, SCALE_TYPES['major'])
    print(f_major)
    assert str(f_major) == 'F major (key sign. -1): F G A Bb C D E F'


def test_b_major_scale():
    b_major = Scale(11, SCALE_TYPES['major'])
    print(b_major)
    assert str(b_major) == 'B major (key sign. 5): B C# D# E F# G# A# B'


def test_a_harmonic_minor_scale():
    a_harmonic_minor = Scale(9, SCALE_TYPES['harmonic_minor'])
    print(a_harmonic_minor)
    assert str(a_harmonic_minor) == 'A harmonic minor (key sign. 0): A B C D E F G# A'


def test_c_harmonic_minor_scale():
    c_harmonic_minor = Scale(0, SCALE_TYPES['harmonic_minor'])
    print(c_harmonic_minor)
    assert str(c_harmonic_minor) == 'C harmonic minor (key sign. -3): C D Eb F G Ab B C'
