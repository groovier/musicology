from musicology.scale import Scale, SCALE_TYPES
from musicology.sequence import Sequence, Frame

# Sequence in C Major.
sequence_1 = Sequence(Scale(0, SCALE_TYPES['major']))
sequence_1.add_event(Frame.from_scale_steps(sequence_1.scale, (0, 4)))
sequence_1.add_event(Frame.from_scale_steps(sequence_1.scale, (1, 4)))
sequence_1.add_event(Frame.from_scale_steps(sequence_1.scale, (2, 4)))
sequence_1.add_event(Frame.from_scale_steps(sequence_1.scale, (3, 4)))
sequence_1.add_event(Frame.from_scale_steps(sequence_1.scale, (4, 4)))
sequence_1.add_event(Frame.from_scale_steps(sequence_1.scale, (3, 4)))
sequence_1.add_event(Frame.from_scale_steps(sequence_1.scale, (2, 4)))
sequence_1.add_event(Frame.from_scale_steps(sequence_1.scale, (1, 4)))
sequence_1.add_event(Frame.from_scale_steps(sequence_1.scale, (0, 4)))
# print(sequence_1)
assert str(sequence_1) == "[['C'], ['D'], ['E'], ['F'], ['G'], ['F'], ['E'], ['D'], ['C']]"
"""
# Sequence in F Major.
sequence_2 = Sequence(Scale(5, SCALE_TYPES['major']))
sequence_2.frame_from_scale_steps((0, 4))
sequence_2.frame_from_scale_steps((1, 4))
sequence_2.frame_from_scale_steps((2, 4))
sequence_2.frame_from_scale_steps((3, 4))
sequence_2.frame_from_scale_steps((4, 4))
sequence_2.frame_from_scale_steps((3, 4))
sequence_2.frame_from_scale_steps((2, 4))
sequence_2.frame_from_scale_steps((1, 4))
sequence_2.frame_from_scale_steps((0, 4))
print(sequence_2)
assert str(sequence_2) == "[['f'], ['g'], ['a'], ['bb'], ['c'], ['bb'], ['a'], ['g'], ['f']]"

# Sequence in E Major.
sequence_3 = Sequence(Scale(4, SCALE_TYPES['major']))
sequence_3.frame_from_scale_steps((0, 4))
sequence_3.frame_from_scale_steps((1, 4))
sequence_3.frame_from_scale_steps((2, 4))
sequence_3.frame_from_scale_steps((3, 4))
sequence_3.frame_from_scale_steps((4, 4))
sequence_3.frame_from_scale_steps((3, 4))
sequence_3.frame_from_scale_steps((2, 4))
sequence_3.frame_from_scale_steps((1, 4))
sequence_3.frame_from_scale_steps((0, 4))
print(sequence_3)
assert str(sequence_3) == "[['e'], ['f#'], ['g#'], ['a'], ['b'], ['a'], ['g#'], ['f#'], ['e']]"
"""
