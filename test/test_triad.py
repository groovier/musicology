from musicology.triad import Triad


def test_scale_step_when_offset_private():
    """Build a scale private Triad with from_offset and assert that it gets the correct scale_step."""
    triad = Triad.from_offset(2, minor=True)
    assert triad.scale_step == 'ii'


def test_scale_step_when_offset_foreign():
    """Build a scale foreign Triad with from_offset and assert that its scale_step is None."""
    triad = Triad.from_offset(2, minor=False)
    assert triad.scale_step is None


def test_scale_step_equals_offset():
    """Test that creating from scale step and from offset results in the same structure."""
    from_offset = Triad.from_offset(5)
    from_scale_step = Triad.from_scale_step('iv')
    assert from_offset.root == from_scale_step.root
